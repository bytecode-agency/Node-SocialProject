# Read me first, please
This is not a representation of my skills in this framework. This is just a copy from my private Bitbucket-repo I made *months* ago. So if you see any rooky mistakes or think it is a very easy project, please take everything with a grain of salt, or a tea spoon. By any means, please don't take this as a measuring tool of what my skills are.

# Social Platform project
Made by Luciano Nooijen

Status: [![Build Status](https://travis-ci.org/lucianonooijen/Node-SocialProject.svg?branch=master)](https://travis-ci.org/lucianonooijen/Node-SocialProject)

## About
This is a copy from my private bitbucket for showcase purposes
This project is built in Node.JS with handlebars templating, and Materialize.css. A MongoDB database is used and the Google+ Auth API is used for authentication.

## How to run
First install the dependencies by running 'npm install'
Rename config/keys_dev_empty.js to config/key.js and change the keys in here
For production, copy the contents from keys_prod.js to keys.js

### Auth keys to enter in the keys_dev.js or keys_prod.js
Google+ API from Google Cloud Platform, OAuth Key, not API key
Authorized JS origins: http://localhost:3000
Autorized redirect URIs: http://localhost:3000/auth/google/callback
